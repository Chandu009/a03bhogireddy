var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app);

// app.get('/', function(request,res){
//   res.send("Guestbook");
// })
// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

app.use(express.static(__dirname + '/assets'));

// manage our entries
// create an array to manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
// set up the http request logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// GETS
// http GET (default and /new-entry)
app.get("/guestbook", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
app.get("/", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/index.html'));
});
app.get("/about", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/About.html'));
});
app.get("/contact", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/Contact.html'));
});
app.get("/pickit", function (request, response) {
  response.sendFile(path.join(__dirname + '/assets/Pickit.html'));
});
// app.get("/guestbook", function (request, response){
//   response.sendFile(path.join(__dirname + 'index'));
// });


// POSTS// http POST (INSERT)
app.post("/contact", function (request, response) {
  var api_key = 'key-f3722f028166cc062a964a72b74c51bb';
  var domain = 'sandboxeedd62214838425d92ea5a12c1136fe4.mailgun.org';
  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

  var data = {
    from: request.body.username +'<postmaster@sandboxeedd62214838425d92ea5a12c1136fe4.mailgun.org>',
    to: 'poornachandu3@gmail.com',
    subject: request.body.username + " sent you a message",
    html: "<b style ='color:Blue'>Message: </b>"+request.body.message
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
      response.send("Mail Sent");
    else
      response.send("Mail not sent");
  });

});

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});


// 404
// 404
app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
