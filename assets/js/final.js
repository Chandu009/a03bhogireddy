var tSum = 0;
var items = document.getElementsByName("music"); 
for(var i=0;i<items.length;i++){
    items[i].addEventListener("click",orderedItems);
}

function orderedItems(){
    tSum = 0;
    //document.getElementById("demo").innerHTML="";
    $('#demo').html("");
    var oItems = document.getElementsByName("music");
    for(var i=0;i<oItems.length;i++){
        if(oItems[i].checked){
            tSum = add(tSum,parseInt(oItems[i].value));
            document.getElementById("demo").innerHTML+="The price of "+ oItems[i].nextSibling.innerHTML.split("-")[0].trim() +" : $"+oItems[i].value+"<br>";
        }
    }
    if(ordered)
        sum();
}

function sum(){
    ordered=true;
    //document.getElementById("ttlPrc").innerHTML = "Total cost of selected items: $" + tSum;
    $('#ttlPrc').html("Total cost of selected items: $" + tSum);
    tax(0.075);
}

function tax(taxRate){
    var taxT = tSum * (1+taxRate);
    document.getElementById("ttlPrc").innerHTML+="<br/>Final cost after tax of 7.5% : $"+taxT;
}

function add(a,b){
	if (typeof a !== 'number' || typeof b !== 'number') {
        throw Error("only numbers are allowed");
    }
    else{
        return a+b;
    } 
}


    